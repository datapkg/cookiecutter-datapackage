#!/usr/bin/env python
import os

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)


def remove_file(filepath):
    os.remove(os.path.join(PROJECT_DIRECTORY, filepath))


def create_symbolic_links():
    for filename in ['AUTHORS.md', 'CONTRIBUTING.md', 'HISTORY.md', 'README.md']:
        try:
            os.symlink(
                '../{}'.format(filename),
                os.path.join(PROJECT_DIRECTORY, 'docs', filename.lower())
            )
        except FileExistsError:
            pass


if __name__ == '__main__':
    create_symbolic_links()

    if '{{ cookiecutter.create_author_file }}' != 'y':
        remove_file('AUTHORS.rst')
        remove_file('docs/authors.rst')

    if 'Not open source' == '{{ cookiecutter.open_source_license }}':
        remove_file('LICENSE')
