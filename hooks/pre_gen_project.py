import re
import sys

MODULE_REGEX = r'^[_a-zA-Z][_\-a-zA-Z0-9]+$'

module_name = '{{ cookiecutter.project_slug }}'

if not re.match(MODULE_REGEX, module_name):
    print(f"ERROR: The project slug ('{module_name}') is not a valid Python "
          f"module name.")

    # Exit to cancel project
    sys.exit(1)

# docs_secret_key = '{{ cookiecutter.docs_secret_key }}'
# 
# if len(docs_secret_key) < 32:
#     print(f"ERROR: The docs secret key ('{docs_secret_key}') is too short! "
#           f"Please use something longer!")
