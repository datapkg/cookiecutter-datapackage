# !/usr/bin/env python
from distutils.core import setup

setup(
    name='cookiecutter-datapkg',
    packages=[],
    version='0.1.0',
    description='Cookiecutter template for a data package',
    author='Alexey Strokach',
    license='BSD',
    author_email='alex.strokach@utoronto.ca',
    url='https://gitlab.com/datapkg/cookiecutter-datapkg',
    keywords=[
        'cookiecutter',
        'template',
        'package',
    ],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Topic :: Software Development',
    ],
)
