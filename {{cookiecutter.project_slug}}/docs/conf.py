"""
Sphinx Configuration File
=========================

For more info, see: http://www.sphinx-doc.org/en/stable/config.html
"""
import sys
import os

import sphinx_bootstrap_theme
from recommonmark.parser import CommonMarkParser
from recommonmark.transform import AutoStructify


def setup(app):
    app.add_stylesheet("css/custom.css")
    app.add_config_value("recommonmark_config", {"enable_eval_rst": True}, True)
    app.add_transform(AutoStructify)


project_root = os.path.dirname(os.path.abspath(__file__))

if project_root not in sys.path:
    sys.path.insert(0, project_root)

extensions = ["sphinx.ext.mathjax"]

templates_path = ["_templates"]

source_suffix = [".rst", ".md"]

source_parsers = {".md": CommonMarkParser}

master_doc = "index"

project = u"{{ cookiecutter.project_name }}"

copyright = u"{% now 'local', '%Y' %}, {{ cookiecutter.full_name }}"

version = "{{ cookiecutter.version }}"

release = "{{ cookiecutter.version }}"

exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "**.ipynb_checkpoints"]

pygments_style = "sphinx"

html_theme = "bootstrap"

html_theme_options = {
    "project_nav_name": project,
    "github_link": (
        "{{ cookiecutter.host_url }}/"
        "{{ cookiecutter.project_namespace }}/"
        "{{ cookiecutter.project_slug }}/"
    ),
}

html_theme_path = sphinx_bootstrap_theme.get_html_theme_path()

html_static_path = ["_static"]

htmlhelp_basename = "{{ cookiecutter.project_slug }}doc"

latex_elements: dict = {}

latex_documents = [
    (
        "index",
        "{{ cookiecutter.project_slug }}.tex",
        u"{{ cookiecutter.project_name }} Documentation",
        u"{{ cookiecutter.full_name }}",
        "manual",
    )
]

man_pages = [
    (
        "index",
        "{{ cookiecutter.project_slug }}",
        u"{{ cookiecutter.project_name }} Documentation",
        [u"{{ cookiecutter.full_name }}"],
        1,
    )
]

texinfo_documents = [
    (
        "index",
        "{{ cookiecutter.project_slug }}",
        u"{{ cookiecutter.project_name }} Documentation",
        u"{{ cookiecutter.full_name }}",
        "{{ cookiecutter.project_slug }}",
        "One line description of project.",
        "Miscellaneous",
    )
]
