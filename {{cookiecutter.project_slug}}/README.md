{% set is_open_source = cookiecutter.open_source_license != 'Not open source' -%}
# {{ cookiecutter.project_name }}

[![docs](https://img.shields.io/badge/docs-v{{ cookiecutter.version }}-blue.svg)](https://{{ cookiecutter.project_namespace }}.gitlab.io/{{ cookiecutter.project_slug }}/{{ cookiecutter.docs_secret_key }})
[![build status]({{ cookiecutter.host_url }}/{{ cookiecutter.project_namespace }}/{{ cookiecutter.project_slug }}/badges/master/build.svg)]({{ cookiecutter.host_url }}/{{ cookiecutter.project_namespace }}/{{ cookiecutter.project_slug }}/commits/master/)

{{ cookiecutter.project_short_description }}

## Notebooks
